package test;

import com.google.common.collect.Lists;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CurlAll {

    static int lowerBound = 1;
    static int upperBound = 181;
    static LocalDateTime from = LocalDateTime.parse("2020-03-10T00:00:00");
    static LocalDateTime to = LocalDateTime.parse("2020-03-14T00:00:00");
    static String logFilename = "euctis_sr_1.log";

    static String format1 = "dd MMM yyyy;HH:mm:ss.SSS";
    static String format2 = "MMM dd, yyyy hh:mm:ss a";
    static String format3 = "'<'dd-MMM-yyyy HH:mm:ss,SSS 'o''clock' 'CET>'";
    static String format4 = "'####<'dd-MMM-yyyy HH:mm:ss,SSS 'o''clock' 'CET>'";
    static DateTimeFormatter formatter1 = null;
    static DateTimeFormatter formatter2 = null;
    static DateTimeFormatter formatter3 = null;
    static DateTimeFormatter formatter4 = null;
    static String localPath = "C:/Users/malekpourkoh/Documents/Sprint16-Exception-Tracking/";

    private static void init() {
        formatter1 = DateTimeFormatter.ofPattern(format1);
        formatter2 = DateTimeFormatter.ofPattern(format2);
        formatter3 = DateTimeFormatter.ofPattern(format3);
        formatter4 = DateTimeFormatter.ofPattern(format4);
    }

    public static void main(String[] args) throws IOException, ParseException, InterruptedException {
        init();

        downloads();

        handle();
    }

    private static void handle() throws IOException, InterruptedException {
        Path report = Paths.get(localPath + "report.txt");
        Files.deleteIfExists(report);
        Files.createFile(report);
        List<Path> paths = Files.list(Paths.get(localPath)).collect(Collectors.toList());
        for (Path path : paths) {
            if(!path.equals(report)){
                System.out.println("Handling " + path.toString());
                ProcessBuilder builder = new ProcessBuilder("C:\\DevTools\\git\\home\\bin\\bash.exe", "-c", "grep -o \"\\.*Exception\" " + path.toString().replace('\\', '/') + " | sort -r | uniq -c");
                builder.redirectErrorStream(true);
                final Process process = builder.start();
                watch(process, path, report);
            }
        }
    }

    private static void downloads() throws IOException {
        String fname = logFilename;
        String fnameWithNumber = null;
        for (int i = lowerBound; i <= upperBound; i++) {
            fnameWithNumber = fname + String.format("%05d", i);
            System.out.println("Processing " + fnameWithNumber);
            ReadableByteChannel readableByteChannel = null;
            try {
                readableByteChannel = Channels.newChannel(new URL("http://logs-test.emea.eu.int/logs_uv1792/euctis_sr_1/" + fnameWithNumber).openStream());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (readableByteChannel != null) {
                String path = localPath + fnameWithNumber;
                FileOutputStream fileOutputStream = new FileOutputStream(path);
                fileOutputStream.getChannel()
                        .transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
                fileOutputStream.close();
                if (!isFileIncludePeriod(path)) {
                    Files.delete(Paths.get(path));
                }
            }
        }
    }

    private static boolean isFileIncludePeriod(String path) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(path));
        Optional<LocalDateTime> first = lines.stream().map(CurlAll::tryParse).filter(Objects::nonNull).findFirst();
        Optional<LocalDateTime> last = Lists.reverse(lines).stream().map(CurlAll::tryParse).filter(Objects::nonNull).findFirst();
        if (first.isPresent() && last.isPresent()) {
            if (to.isAfter(first.get()) && last.get().isAfter(from)) {
                return true;
            }
        } else {
            throw new RuntimeException("This path can not be processed : " + path);
        }
        return false;
    }

    private static LocalDateTime tryParse(String line) {
        LocalDateTime localDateTime = parse(line, format1, formatter1);
        localDateTime = localDateTime == null ? parse(line, format2, formatter2) : localDateTime;
        localDateTime = localDateTime == null ? parse(line, format3.replace("''", "1").replace("'",""), formatter3) : localDateTime;
        localDateTime = localDateTime == null ? parse(line, format4.replace("''", "1").replace("'",""), formatter4) : localDateTime;
        return localDateTime;
    }

    private static LocalDateTime parse(String line, String format, DateTimeFormatter formatter) {
        try {
            return LocalDateTime.parse(line.substring(0, format.toString().length()), formatter);
        } catch (DateTimeParseException | StringIndexOutOfBoundsException ex) {
            return null;
        }

    }

    private static void watch(final Process process, Path path, Path report) throws InterruptedException {
        new Thread() {
            public void run() {
                try {
                    BufferedReader stdInput = new BufferedReader(new
                            InputStreamReader(process.getInputStream()));

                    BufferedReader stdError = new BufferedReader(new
                            InputStreamReader(process.getErrorStream()));

                    // Read the output from the command

                    Files.write(report, ("\n\n\n------------- " + path.getFileName().toString() + " -----------\n\n").getBytes(), StandardOpenOption.APPEND);
                    String s = null;
                    while ((s = stdInput.readLine()) != null) {
                        Files.write(report, (s + "\n").getBytes(), StandardOpenOption.APPEND);
                    }
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }.start();
        process.waitFor();
    }
}
