package com.example.authorizationdesign.config;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @SneakyThrows
    @Autowired
    public void initialize(AuthenticationManagerBuilder builder) {
        builder.inMemoryAuthentication().withUser("hadi").password("{noop}hadi").authorities("basic");
        builder.inMemoryAuthentication().withUser("vald").password("{noop}vald").authorities("admin");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        super.configure(http);
        http.csrf().disable();
    }
}
