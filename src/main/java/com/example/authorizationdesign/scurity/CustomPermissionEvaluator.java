package com.example.authorizationdesign.scurity;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;
import java.util.Arrays;

public class CustomPermissionEvaluator implements PermissionEvaluator {
    @Override
    public boolean hasPermission(Authentication authentication, Object accessType, Object permission) {
        if (authentication != null && accessType instanceof String) {
            if ("hasAccess".equalsIgnoreCase(String.valueOf(accessType))) {
                return validateAccess(authentication, String.valueOf(permission));
            }
        }
        return false;
    }

    private boolean validateAccess(Authentication authentication, String permission) {
        for (final GrantedAuthority grantedAuth : authentication.getAuthorities()) {
            if (grantedAuth.getAuthority().equals("admin")) {
                return Arrays.asList("read", "write").contains(permission);
            } else {
                return Arrays.asList("read").contains(permission);
            }
        }
        return false;
    }

    @Override
    public boolean hasPermission(Authentication authentication, Serializable serializable, String targetType, Object permission) {
        return false;
    }
}