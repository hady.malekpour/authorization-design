package com.example.authorizationdesign.web;

import com.example.authorizationdesign.dto.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class OrderController {
    @RequestMapping(path = "/api/order/{id}", method = RequestMethod.GET)
    @PreAuthorize("hasPermission('hasAccess', 'read')")
    public Order get(@PathVariable Long id) {
        log.debug("GET method has been called");
        return new Order(id);
    }

    @RequestMapping(path = "/api/order/{id}", method = RequestMethod.DELETE)
    @PreAuthorize("hasPermission('hasAccess', 'write')")
    public void delete(@PathVariable Long id) {
        log.debug("DELETE method has been called");
    }
}
