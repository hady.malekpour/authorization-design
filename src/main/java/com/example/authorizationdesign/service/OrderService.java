package com.example.authorizationdesign.service;

import com.example.authorizationdesign.dto.Order;

public class OrderService {
    public Order get(Long id) {
        return new Order(id);
    }
}
